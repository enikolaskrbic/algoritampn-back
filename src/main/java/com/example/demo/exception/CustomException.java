package com.example.demo.exception;

import lombok.Getter;

@Getter
public class CustomException extends Exception {

    private String resourceBundleExceptionKey;

    protected CustomException(String resourceBundleExceptionKey) {
        this.resourceBundleExceptionKey = resourceBundleExceptionKey;
    }
}
