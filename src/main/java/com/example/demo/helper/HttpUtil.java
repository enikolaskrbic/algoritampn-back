package com.example.demo.helper;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

public final class HttpUtil {

    private HttpUtil() {

    }

    public static Locale getLocaleFromHeader(HttpServletRequest httpServletRequest) {
        final String languageCode = httpServletRequest.getHeader("Language");
        return LanguageUtil.getLocaleFromLanguageCode(languageCode);
    }
}
