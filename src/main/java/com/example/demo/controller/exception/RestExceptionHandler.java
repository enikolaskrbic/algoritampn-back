package com.example.demo.controller.exception;

import com.example.demo.exception.CustomException;
import com.example.demo.service.user.exception.ApiKeyNotValidException;
import com.example.demo.service.user.exception.CredentialsInvalidException;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.util.Locale.ENGLISH;
import static org.springframework.http.HttpStatus.*;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = {
            ApiKeyNotValidException.class
    })
    public ResponseEntity handleConflictException(CustomException e, Locale locale) {
        return buildResponseEntity(new ExceptionResponse(e.getResourceBundleExceptionKey(), locale, CONFLICT));
    }

    @ExceptionHandler(value = {
            UserDoesntExistsException.class
    })
    public ResponseEntity handleNotFoundException(CustomException e, Locale locale) {
        return buildResponseEntity(new ExceptionResponse(e.getResourceBundleExceptionKey(), locale, NOT_FOUND));
    }

    @ExceptionHandler(value = {
            CredentialsInvalidException.class
//            VerificationCodeInvalidException.class,
//            VerificationTokenInvalidException.class,
//            EmailInvalidException.class
    })
    public ResponseEntity handleUnauthorizedException(CustomException e, Locale locale) {
        logger.error("Handling access denied error - {}", e.getMessage(), e);
        return buildResponseEntity(new ExceptionResponse(e.getResourceBundleExceptionKey(), locale, UNAUTHORIZED));
    }

//    @ExceptionHandler(value = {
//            ListingUpdateForbiddenException.class,
//            ListingAlreadyReportedByUserForbiddenException.class,
//    })
//    public ResponseEntity handleForbiddenException(CustomException e, Locale locale) {
//        logger.error("Handling forbidden error - {}", e.getMessage(), e);
//        return buildResponseEntity(new ExceptionResponse(e.getResourceBundleExceptionKey(), locale, FORBIDDEN));
//    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    public ResponseEntity handleAccessDeniedException(Exception e, Locale locale) {
        logger.error("Handling access denied error - {}", e.getMessage(), e);
        return buildResponseEntity(new ExceptionResponse("access.denied", locale, FORBIDDEN));
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity handleException(Exception e, Locale locale) {
        logger.error("Handling unknown error - {}", e.getMessage(), e);
        return buildResponseEntity(new ExceptionResponse("internal.server.error", locale, e.getMessage(), INTERNAL_SERVER_ERROR));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling method argument not valid error - {}", ex.getMessage(), ex);
        ExceptionResponse response = new ExceptionResponse("invalid.input", ENGLISH, BAD_REQUEST);
        response.setErrors(fromBindingErrors(ex.getBindingResult()));
        return buildResponseEntity(response);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling missing servlet request parameter error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling http request method not supported error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling http media type not supported error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling http media type not acceptable error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling missing path variable error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling servlet request binding exception - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling conversion not supported error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling type mismatch error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling http message not readable error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling http message not writable error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling missing servlet request part error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("invalid.input", ENGLISH, ex.getMessage(), BAD_REQUEST));
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling bind exception error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("internal.server.error", ENGLISH, ex.getMessage(), INTERNAL_SERVER_ERROR));
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling no handler found exception - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("internal.server.error", ENGLISH, ex.getMessage(), INTERNAL_SERVER_ERROR));
    }

    @Override
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        logger.error("Handling async request timeout exception - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("internal.server.error", ENGLISH, ex.getMessage(), INTERNAL_SERVER_ERROR));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("Handling exception internal error - {}", ex.getMessage(), ex);
        return buildResponseEntity(new ExceptionResponse("internal.server.error", ENGLISH, ex.getMessage(), INTERNAL_SERVER_ERROR));
    }

    private ResponseEntity<Object> buildResponseEntity(ExceptionResponse exceptionResponse) {
        return new ResponseEntity<>(exceptionResponse, valueOf(exceptionResponse.getStatus()));
    }

    private List<String> fromBindingErrors(Errors errors) {
        List<String> validErrors = new ArrayList<>();
        for (ObjectError objectError : errors.getAllErrors()) {
            validErrors.add(((FieldError) objectError).getField() + " " + objectError.getDefaultMessage());
        }
        return validErrors;
    }
}
