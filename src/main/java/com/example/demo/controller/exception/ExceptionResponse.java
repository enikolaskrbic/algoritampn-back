package com.example.demo.controller.exception;

import com.example.demo.helper.ResourceBundleUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Locale;

@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ExceptionResponse {

    @JsonProperty("status")
    private int status;

    @JsonProperty("status_text")
    private String statusText;

    @JsonProperty("localized_error_message")
    private String localizedErrorMessage;

    @JsonProperty("error_description")
    private String errorDescription;

    private List<String> data;

    public ExceptionResponse(String resourceBundleExceptionKey, Locale locale, HttpStatus status) {
        this();
        this.status = status.value();
        this.statusText = status.getReasonPhrase();
        this.localizedErrorMessage = ResourceBundleUtil.getExceptionValue(locale, resourceBundleExceptionKey);
        this.errorDescription = ResourceBundleUtil.getExceptionValue(Locale.ENGLISH, resourceBundleExceptionKey);
    }

    public ExceptionResponse(String resourceBundleExceptionKey, Locale locale, String errorDescription, HttpStatus status) {
        this();
        this.status = status.value();
        this.statusText = status.getReasonPhrase();
        this.localizedErrorMessage = ResourceBundleUtil.getExceptionValue(locale, resourceBundleExceptionKey);
        this.errorDescription = errorDescription;
    }

    void setErrors(List<String> data) {
        this.data = data;
    }
}
