package com.example.demo.controller;

import com.example.demo.controller.dto.InvoiceRequest;
import com.example.demo.controller.dto.SaveInvoiceRequest;
import com.example.demo.controller.dto.response.efaktura.UploadInvoiceResponse;
import com.example.demo.service.invoice.*;
import com.example.demo.service.user.exception.ApiKeyNotValidException;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@EnableAutoConfiguration
@RequestMapping("/api")
@CrossOrigin
public class InvoiceController {

    private final String EFAKTURA_BASE_URL = "https://demoefaktura.mfin.gov.rs/";

    @Autowired
    private CreateInvoicesUseCase createInvoicesUseCase;

    @Autowired
    private SaveInvoiceUseCase saveInvoiceUseCase;

    @Autowired
    private GetInvoicesUseCase getInvoicesUseCase;

    @Autowired
    private GetInvoiceXmlUseCase getInvoiceXmlUseCase;

    @Autowired
    private GetAndSaveInvoiceStatus getAndSaveInvoiceStatus;



//    @RequestMapping(value = "/invoices", method = POST)
//    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
//    public ResponseEntity createInvoices(@RequestBody @Valid InvoiceRequest invoiceRequest, @ApiIgnore @AuthenticationPrincipal Long principalId) throws  UserDoesntExistsException {
//        createInvoicesUseCase.execute(invoiceRequest, principalId);
//        return new ResponseEntity<>(HttpStatus.CREATED);
//    }


    @RequestMapping(value = "/invoices", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public ResponseEntity getTrainings(@RequestParam(value = "fromDate") Long fromDate,
                                       @RequestParam(value = "toDate") Long toDate,
                                       @ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException {
        return new ResponseEntity<>(getInvoicesUseCase.execute(principalId, fromDate, toDate), HttpStatus.OK);
    }

    @RequestMapping(value = "/invoices/xml/{id}", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public ResponseEntity getInvoiceXml(@PathVariable(value = "id") Long id,
                                        @ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException {
        return new ResponseEntity<>(getInvoiceXmlUseCase.execute(principalId, id), HttpStatus.OK);
    }

    @RequestMapping(value = "/invoices/efaktura/{id}", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public ResponseEntity getInvoiceEfaktura(@PathVariable(value = "id") Long id,
                                             @RequestParam(value = "ApiKey") String apiKey,
                                             @ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException, URISyntaxException, ApiKeyNotValidException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.add("ApiKey", apiKey);
        headers.add("accept", "application/json, text/plain");
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        URI uri = new URI(EFAKTURA_BASE_URL + "api/publicApi/sales-invoice?invoiceId="+id);
        try {
            ResponseEntity responseEntity =  restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class);
            checkStatusFromEfaktura(responseEntity.getStatusCode().value());
            getAndSaveInvoiceStatus.execute(responseEntity, id);
            return responseEntity;
        } catch(HttpStatusCodeException e) {
            checkStatusFromEfaktura(e.getRawStatusCode());
            return ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
        }
    }


    @RequestMapping(value = "/ubl", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public @ResponseBody ResponseEntity uploadInvoice(@RequestParam(value = "requestId") String requestId,
                                                 @RequestParam(value = "ApiKey") String apiKey,
                                                 @RequestParam(value = "fileName") String fileName,
                                                 @RequestParam(value = "uploadTime") Long uploadTime,
                                                 @ApiIgnore @AuthenticationPrincipal Long principalId,
                                                 @RequestBody String body,
                                                 final HttpServletResponse response,final HttpServletRequest request) throws URISyntaxException, ApiKeyNotValidException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.add("ApiKey", apiKey);
        headers.add("accept", "application/json, text/plain");
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);
        URI uri2 = new URI(EFAKTURA_BASE_URL + "api/publicApi/sales-invoice/ubl?requestId="+requestId);
        try {
            ResponseEntity responseEntity = restTemplate.exchange(uri2, HttpMethod.POST, httpEntity, String.class);
            checkStatusFromEfaktura(responseEntity.getStatusCode().value());
            if(responseEntity.getStatusCode().is2xxSuccessful()){
                try {
                    Gson gson = new Gson();
                    UploadInvoiceResponse u = gson.fromJson(String.valueOf(responseEntity.getBody()), UploadInvoiceResponse.class);
                    SaveInvoiceRequest saveInvoiceRequest = new SaveInvoiceRequest();
                    saveInvoiceRequest.setInvoiceId(u.getInvoiceId());
                    saveInvoiceRequest.setSalesId(u.getSalesInvoiceId());
                    saveInvoiceRequest.setFileName(fileName);
                    saveInvoiceRequest.setUploadXml(body);
                    saveInvoiceRequest.setUploadedTime(uploadTime);
                    saveInvoiceUseCase.execute(saveInvoiceRequest, principalId);
                }catch (Exception e){
//                    System.out.println("RESPONSE ERROR: " +  e.getMessage());
                }
            }
            return responseEntity;
        } catch(HttpStatusCodeException e) {
            checkStatusFromEfaktura(e.getRawStatusCode());
            return ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
        }
    }

    private void checkStatusFromEfaktura(int status) throws ApiKeyNotValidException {
        if(status == 401){
            throw new ApiKeyNotValidException();
        }
    }
}
