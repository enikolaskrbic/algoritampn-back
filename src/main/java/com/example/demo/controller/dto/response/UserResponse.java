package com.example.demo.controller.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String code;

    private String name;

    private String username;

    private String email;

    private String apiKey;

    private Boolean isEnabledMojeracun;

}
