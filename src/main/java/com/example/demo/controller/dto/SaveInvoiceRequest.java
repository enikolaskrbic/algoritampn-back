package com.example.demo.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SaveInvoiceRequest {

    @JsonProperty("fileName")
    @NotNull
    @ApiModelProperty(required = true)
    private String fileName;

    @JsonProperty("invoiceId")
    @NotNull
    @ApiModelProperty(required = true)
    private Long invoiceId;

    @JsonProperty("salesId")
    @NotNull
    @ApiModelProperty(required = true)
    private Long salesId;

    @JsonProperty("uploadedTime")
    @NotNull
    @ApiModelProperty(required = true)
    private Long uploadedTime;

    @JsonProperty("uploadedXml")
    @NotNull
    @ApiModelProperty(required = true)
    private String uploadXml;

}
