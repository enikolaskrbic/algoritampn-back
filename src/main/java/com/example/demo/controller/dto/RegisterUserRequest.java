package com.example.demo.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterUserRequest implements Serializable {

    @JsonProperty("code")
    @NotNull
    @ApiModelProperty(required = true)
    private String code;

    @JsonProperty("name")
    @NotNull
    @ApiModelProperty(required = true)
    private String name;

    @JsonProperty("username")
    @NotNull
    @ApiModelProperty(required = true)
    private String username;

    @JsonProperty("password")
    @NotNull
    @ApiModelProperty(required = true)
    private String password;

    @JsonProperty("email")
    @NotNull
    @ApiModelProperty(required = true)
    private String email;

    @JsonProperty("apiKey")
    @NotNull
    @ApiModelProperty(required = true)
    private String apiKey;

    @JsonProperty("isEnabledMojeracun")
    @NotNull
    @ApiModelProperty(required = true)
    private Boolean isEnabledMojeracun;

}
