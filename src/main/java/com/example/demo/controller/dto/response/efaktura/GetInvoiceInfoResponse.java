package com.example.demo.controller.dto.response.efaktura;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetInvoiceInfoResponse {

    private String Status;
}
