package com.example.demo.controller.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long invoiceId;

    private Long salesId;

    private String fileName;

    private Long uploadTime;

    private String status;

}
