package com.example.demo.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdateApiKeyRequest {

    @JsonProperty("apiKey")
    @NotNull
    @ApiModelProperty(required = true)
    private String apyKey;

}
