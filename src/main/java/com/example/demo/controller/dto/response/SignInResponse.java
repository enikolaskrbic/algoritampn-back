package com.example.demo.controller.dto.response;

import com.example.demo.domain.model.enumeration.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String accessToken;

    private String apiKey;

    private String name;

    private String username;

    private Role role;


}
