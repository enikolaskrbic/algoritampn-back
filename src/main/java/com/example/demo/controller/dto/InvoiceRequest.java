package com.example.demo.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InvoiceRequest implements Serializable {

    @JsonProperty("invoices")
    @NotNull
    @ApiModelProperty(required = true)
    private List<InvoiceItemRequest> invoices;

}

