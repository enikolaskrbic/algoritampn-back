package com.example.demo.controller;

import com.example.demo.controller.dto.RegisterUserRequest;
import com.example.demo.controller.dto.UpdateApiKeyRequest;
import com.example.demo.controller.dto.UpdatePasswordRequest;
import com.example.demo.controller.dto.UpdateUserRequest;
import com.example.demo.domain.model.enumeration.Role;
import com.example.demo.service.invoice.GetUserInvoicesUseCase;
import com.example.demo.service.user.*;
import com.example.demo.service.user.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@EnableAutoConfiguration
@RequestMapping("/api")
@CrossOrigin
public class UserController {

    @Autowired
    private AddAdminUseCase addAdminUseCase;

    @Autowired
    private GetUserInvoicesUseCase getUserInvoicesUseCase;

    @Autowired
    private RegisterUserUseCase registerUserUseCase;

    @Autowired
    private ResetPasswordUseCase resetPasswordUseCase;

    @Autowired
    private DeleteUserUseCase deleteUserUseCase;

    @Autowired
    private GetUsersUseCase getUsersUseCase;

    @Autowired
    private UpdatePasswordUseCase updatePasswordUseCase;

    @Autowired
    private UpdateApiKeyUseCase updateApiKeyUseCase;

    @Autowired
    private UpdateUserUseCase updateUserUseCase;

    @RequestMapping(value = "/add-admin", method = POST)
    public ResponseEntity addAdmin() throws UserDoesntExistsException, UsernameAlreadyExistsException {
        RegisterUserRequest r = new RegisterUserRequest();
        r.setEmail("brankica@gmail.com");
        r.setApiKey("dasdsadsadasd");
        r.setPassword("bane1234");
        r.setUsername("brankica");
        r.setCode("Brankica");
        r.setName("Stefanovic");
        r.setApiKey("5c46edc6-44c6-403d-8768-9f0d80dd7fb3");
        addAdminUseCase.execute(r, Role.ROLE_ADMIN);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') ")
    public ResponseEntity getUsers(@ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException {
        return new ResponseEntity<>(getUsersUseCase.execute(principalId), HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = POST)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity registerUser(@RequestBody @Valid RegisterUserRequest registerUserRequest, @ApiIgnore @AuthenticationPrincipal Long principalId) throws  UsernameAlreadyExistsException, UserDoesntExistsException {
        registerUserUseCase.execute(registerUserRequest, principalId, Role.ROLE_USER);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }



    @RequestMapping(value = "/users/{id}/reset-password", method = PATCH)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity resetPassword(@PathVariable Long id,
                                        @ApiIgnore @AuthenticationPrincipal Long principalId) throws OldPasswordInvalidException, UserDoesntExistsException {
        resetPasswordUseCase.execute(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/users/{id}", method = DELETE)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity deleteUser(@PathVariable Long id, @ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException {
        deleteUserUseCase.execute(id, principalId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/users/password", method = PATCH)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public ResponseEntity changePassword(@RequestBody @Valid UpdatePasswordRequest payload,
                                         @ApiIgnore @AuthenticationPrincipal Long principalId) throws OldPasswordInvalidException, UserDoesntExistsException, NewPasswordEmptyException, NewPasswordEmptyException {
        updatePasswordUseCase.execute(payload, principalId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/users/apikey", method = PATCH)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') ")
    public ResponseEntity updateApiKey(@RequestBody @Valid UpdateApiKeyRequest payload,
                                         @ApiIgnore @AuthenticationPrincipal Long principalId) throws OldPasswordInvalidException, UserDoesntExistsException, NewPasswordEmptyException, NewPasswordEmptyException {
        updateApiKeyUseCase.execute(payload, principalId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/users/{id}", method = PUT)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_USER')")
    public ResponseEntity updateInfo(@PathVariable Long id, @RequestBody @Valid UpdateUserRequest payload, @ApiIgnore @AuthenticationPrincipal Long principalId) throws EmailAlreadyExistsException, UsernameAlreadyExistsException, UserDoesntExistsException {
        updateUserUseCase.execute(payload,id, principalId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/user-report", method = GET)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') ")
    public ResponseEntity getUserReport(@RequestParam(value = "fromDate") Long fromDate,
                                       @RequestParam(value = "toDate") Long toDate,
                                       @ApiIgnore @AuthenticationPrincipal Long principalId) throws UserDoesntExistsException {
        return new ResponseEntity<>(getUserInvoicesUseCase.execute(principalId, fromDate, toDate), HttpStatus.OK);
    }

}
