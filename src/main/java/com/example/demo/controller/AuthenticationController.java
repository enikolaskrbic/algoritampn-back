package com.example.demo.controller;

import com.example.demo.controller.dto.SignInRequest;
import com.example.demo.service.auth.SignInUseCase;
import com.example.demo.service.user.exception.CredentialsInvalidException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AuthenticationController {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationController.class.getName());

    @Autowired
    private SignInUseCase signInUseCase;

    @RequestMapping(value = "/authentication/sign-in", method = POST)
    public ResponseEntity signIn(@RequestBody @Valid SignInRequest payload) throws CredentialsInvalidException {
        LOGGER.info("POST /authentication/sign-in");
        return new ResponseEntity<>(signInUseCase.execute(payload), HttpStatus.OK);
    }

}
