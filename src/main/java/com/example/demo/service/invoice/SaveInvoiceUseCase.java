package com.example.demo.service.invoice;

import com.example.demo.controller.dto.SaveInvoiceRequest;
import com.example.demo.domain.model.Invoice;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.InvoiceRepository;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class SaveInvoiceUseCase {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private UserRepository userRepository;

    public void execute(SaveInvoiceRequest request, Long principalId) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }
        invoiceRepository.save(generate(request, user.get()));
    }

    private Invoice generate(SaveInvoiceRequest i, User user){
        Invoice invoice = new Invoice();
        invoice.setInvoiceId(i.getInvoiceId());
        invoice.setSalesId(i.getSalesId());
        invoice.setFileName(i.getFileName());
        invoice.setUploadTime(i.getUploadedTime());
        invoice.setUploadXml(i.getUploadXml());
        invoice.setStatus("Sent");
        invoice.setUser(user);
        return invoice;
    }
}
