package com.example.demo.service.invoice;


import com.example.demo.controller.dto.response.InvoiceResponse;
import com.example.demo.domain.model.Invoice;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.InvoiceRepository;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GetInvoiceXmlUseCase {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private UserRepository userRepository;

    public String execute(Long principalId, Long invoiceId) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

        Optional<Invoice> invoice = Optional.ofNullable(invoiceRepository.getById(invoiceId));
        if (!invoice.isPresent()) {
            throw new UserDoesntExistsException();
        }

        return invoice.get().getUploadXml();
    }

}
