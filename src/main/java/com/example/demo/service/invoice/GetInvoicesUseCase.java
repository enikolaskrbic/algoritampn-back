package com.example.demo.service.invoice;


import com.example.demo.controller.dto.InvoiceItemRequest;
import com.example.demo.controller.dto.InvoiceRequest;
import com.example.demo.controller.dto.response.InvoiceResponse;
import com.example.demo.domain.model.Invoice;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.InvoiceRepository;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GetInvoicesUseCase {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private UserRepository userRepository;

    public List<InvoiceResponse> execute(Long principalId, Long fromTime, Long toTime) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

        return invoiceRepository.getByFromDateToDateAndUser(fromTime, toTime, user.get().getId());
    }

}
