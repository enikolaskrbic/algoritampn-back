package com.example.demo.service.invoice;


import com.example.demo.controller.dto.InvoiceItemRequest;
import com.example.demo.controller.dto.InvoiceRequest;
import com.example.demo.controller.dto.response.UserResponse;
import com.example.demo.domain.model.Invoice;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.InvoiceRepository;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class CreateInvoicesUseCase {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private UserRepository userRepository;

    public void execute(InvoiceRequest invoiceRequest, Long principalId) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }
        List<Invoice> list = new ArrayList<>();
        for(int i=0; i<invoiceRequest.getInvoices().size(); i++){
            list.add(generate(invoiceRequest.getInvoices().get(i), user.get()));
        }
        invoiceRepository.saveAll(list);
    }

    private Invoice generate(InvoiceItemRequest i, User user){
        Invoice invoice = new Invoice();
        invoice.setInvoiceId(i.getInvoiceId());
        invoice.setSalesId(i.getSalesId());
        invoice.setFileName(i.getFileName());
        invoice.setUploadTime(i.getUploadedTime());
        invoice.setUser(user);
        return invoice;
    }

}
