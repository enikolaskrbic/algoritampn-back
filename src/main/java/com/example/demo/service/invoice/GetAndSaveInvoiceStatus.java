package com.example.demo.service.invoice;

import com.example.demo.controller.dto.response.efaktura.GetInvoiceInfoResponse;
import com.example.demo.domain.model.Invoice;
import com.example.demo.domain.repository.InvoiceRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GetAndSaveInvoiceStatus {
    @Autowired
    private InvoiceRepository invoiceRepository;

    public void execute(ResponseEntity responseEntity, Long salesId) throws UserDoesntExistsException {
        Optional<Invoice> invoice = Optional.ofNullable(invoiceRepository.getBySalesId(salesId));
        if(!invoice.isPresent()) return;
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            try {
                Gson gson = new Gson();
                GetInvoiceInfoResponse infoResponse = gson.fromJson(String.valueOf(responseEntity.getBody()), GetInvoiceInfoResponse.class);
                invoice.get().setStatus(infoResponse.getStatus());
                invoiceRepository.save(invoice.get());
            }catch (Exception e){
//                System.out.println("RESPONSE ERROR: " +  e.getMessage());
            }
        }
    }
}
