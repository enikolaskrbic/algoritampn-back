package com.example.demo.service.invoice;

import com.example.demo.controller.dto.response.UserInvoicesResponse;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GetUserInvoicesUseCase {

    @Autowired
    private UserRepository userRepository;

    public List<UserInvoicesResponse> execute(Long principalId, Long fromTime, Long toTime) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

        return userRepository.getUsersInvoices(principalId, fromTime, toTime);
    }

}