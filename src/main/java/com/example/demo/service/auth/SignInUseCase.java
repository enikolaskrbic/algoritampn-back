package com.example.demo.service.auth;

import com.example.demo.configuration.security.JWTUtils;
import com.example.demo.controller.dto.SignInRequest;
import com.example.demo.controller.dto.response.SignInResponse;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.CredentialsInvalidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class SignInUseCase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public SignInResponse execute(SignInRequest signInRequest) throws CredentialsInvalidException {
        Optional<User> user = Optional.ofNullable(userRepository.getByUsernameAndDeleted(signInRequest.getUsername(), false));
        if (!user.isPresent()) {
            throw new CredentialsInvalidException();
        }
        if (!passwordEncoder.matches(signInRequest.getPassword(), user.get().getPassword())) {
            throw new CredentialsInvalidException();
        }

        final String accessToken = JWTUtils.generateToken(user.get());

        final SignInResponse response = new SignInResponse();
        response.setId(user.get().getId());
        response.setAccessToken(accessToken);
        response.setApiKey(user.get().getApiKey());
        response.setUsername(user.get().getUsername());
        response.setName(user.get().getName());
        response.setRole(user.get().getRole());
        return response;
    }
}
