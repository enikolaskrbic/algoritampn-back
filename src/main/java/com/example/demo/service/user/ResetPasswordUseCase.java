package com.example.demo.service.user;

import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ResetPasswordUseCase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void execute(Long id) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(id));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }
        user.get().setPassword(passwordEncoder.encode("12345678"));
        userRepository.save(user.get());
    }
}
