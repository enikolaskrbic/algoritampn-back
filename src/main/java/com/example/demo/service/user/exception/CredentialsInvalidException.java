package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class CredentialsInvalidException extends CustomException {
    public CredentialsInvalidException() {
        super("credentials.invalid");
    }
}
