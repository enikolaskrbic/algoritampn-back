package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class OldPasswordInvalidException extends CustomException {
    public OldPasswordInvalidException() {
        super("old.password.invalid");
    }
}