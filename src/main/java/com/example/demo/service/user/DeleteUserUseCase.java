package com.example.demo.service.user;

import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class DeleteUserUseCase {

    @Autowired
    private UserRepository userRepository;

    public void execute(Long id, Long adminId) throws UserDoesntExistsException {
        if(id.equals(adminId)){
            throw new UserDoesntExistsException();
        }

        Optional<User> user = Optional.ofNullable(userRepository.getByIdAndDeleted(id,false));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

        user.get().setDeleted(true);
        userRepository.save(user.get());
    }
}
