package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class NewPasswordEmptyException extends CustomException {
    public NewPasswordEmptyException() {
        super("new.password.empty");
    }
}