package com.example.demo.service.user;

import com.example.demo.controller.dto.RegisterUserRequest;
import com.example.demo.domain.model.User;
import com.example.demo.domain.model.enumeration.Role;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import com.example.demo.service.user.exception.UsernameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class AddAdminUseCase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void execute(RegisterUserRequest registerUserValue, Role role) throws UserDoesntExistsException, UsernameAlreadyExistsException {

        User user = new User();
        user.setCode(registerUserValue.getCode());
        user.setName(registerUserValue.getName());
        user.setApiKey(registerUserValue.getApiKey());
        user.setUsername(registerUserValue.getUsername());
        user.setEmail(registerUserValue.getEmail());
        user.setPassword(passwordEncoder.encode(registerUserValue.getPassword()));
        user.setRole(role);
        user.setDeleted(false);
        user.setIsEnabledMojeracun(false);
        userRepository.save(user);
    }


}
