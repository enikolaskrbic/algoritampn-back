package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class EmailAlreadyExistsException extends CustomException {
    public EmailAlreadyExistsException() {
        super("email.exists");
    }
}
