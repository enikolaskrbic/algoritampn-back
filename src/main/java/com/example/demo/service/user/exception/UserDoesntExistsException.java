package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class UserDoesntExistsException extends CustomException {
    public UserDoesntExistsException() {
        super("user.not.found");
    }
}
