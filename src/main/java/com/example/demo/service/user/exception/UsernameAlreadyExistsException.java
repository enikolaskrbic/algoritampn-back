package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class UsernameAlreadyExistsException extends CustomException {
    public UsernameAlreadyExistsException() {
        super("username.exists");
    }
}
