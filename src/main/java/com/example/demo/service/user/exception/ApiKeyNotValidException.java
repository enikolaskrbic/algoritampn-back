package com.example.demo.service.user.exception;

import com.example.demo.exception.CustomException;

public class ApiKeyNotValidException extends CustomException {
    public ApiKeyNotValidException() {
        super("api.key.not.valid");
    }
}
