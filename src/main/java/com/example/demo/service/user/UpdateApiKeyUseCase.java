package com.example.demo.service.user;


import com.example.demo.controller.dto.UpdateApiKeyRequest;
import com.example.demo.controller.dto.UpdatePasswordRequest;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.NewPasswordEmptyException;
import com.example.demo.service.user.exception.OldPasswordInvalidException;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class UpdateApiKeyUseCase {

    @Autowired
    private UserRepository userRepository;

    public void execute(UpdateApiKeyRequest updateApiKeyRequest, Long principalId) throws UserDoesntExistsException, OldPasswordInvalidException, NewPasswordEmptyException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }
        user.get().setApiKey(updateApiKeyRequest.getApyKey());
        userRepository.save(user.get());
    }
}
