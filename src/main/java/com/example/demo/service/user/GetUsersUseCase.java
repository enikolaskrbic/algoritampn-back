package com.example.demo.service.user;


import com.example.demo.controller.dto.response.UserResponse;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GetUsersUseCase {

    @Autowired
    private UserRepository userRepository;

    public List<UserResponse> execute(Long principalId) throws UserDoesntExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

       return userRepository.getUsers(user.get().getId());
    }

}
