package com.example.demo.service.user;


import com.example.demo.controller.dto.UpdatePasswordRequest;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.NewPasswordEmptyException;
import com.example.demo.service.user.exception.OldPasswordInvalidException;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class UpdatePasswordUseCase {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void execute(UpdatePasswordRequest updatePasswordRequest, Long principalId) throws UserDoesntExistsException, OldPasswordInvalidException, NewPasswordEmptyException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(principalId));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }

        if(updatePasswordRequest.getNewPassword().trim().isEmpty()){
            throw new NewPasswordEmptyException();
        }

        if (!passwordEncoder.matches(updatePasswordRequest.getOldPassword(), user.get().getPassword())) {
            throw new OldPasswordInvalidException();
        }

        user.get().setPassword(passwordEncoder.encode(updatePasswordRequest.getNewPassword()));
        userRepository.save(user.get());
    }
}
