package com.example.demo.service.user;

import com.example.demo.controller.dto.UpdateUserRequest;
import com.example.demo.domain.model.User;
import com.example.demo.domain.repository.UserRepository;
import com.example.demo.service.user.exception.EmailAlreadyExistsException;
import com.example.demo.service.user.exception.UserDoesntExistsException;
import com.example.demo.service.user.exception.UsernameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class UpdateUserUseCase {

    @Autowired
    private UserRepository userRepository;

    public void execute(UpdateUserRequest updateUserRequest,Long idToChange, Long id) throws UserDoesntExistsException, UsernameAlreadyExistsException, EmailAlreadyExistsException {
        Optional<User> user = Optional.ofNullable(userRepository.getById(id));
        if (!user.isPresent()) {
            throw new UserDoesntExistsException();
        }
        Optional<User> userToChange = Optional.ofNullable(userRepository.getById(idToChange));
        if (!userToChange.isPresent()) {
            throw new UserDoesntExistsException();
        }

        Optional<User> user1 = Optional.ofNullable(userRepository.getByUsernameAndDeleted(updateUserRequest.getUsername(), false));
        if(user1.isPresent() && !user1.get().getId().equals(userToChange.get().getId())){
            throw new UsernameAlreadyExistsException();
        }

        userToChange.get().setCode(updateUserRequest.getCode());
        userToChange.get().setName(updateUserRequest.getName());
        userToChange.get().setUsername(updateUserRequest.getUsername());
        userToChange.get().setEmail(updateUserRequest.getEmail());
        userToChange.get().setApiKey(updateUserRequest.getApiKey());
        userToChange.get().setIsEnabledMojeracun(updateUserRequest.getIsEnabledMojeracun());
        userRepository.save(userToChange.get());
    }
}
