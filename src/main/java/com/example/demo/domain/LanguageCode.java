package com.example.demo.domain;

public enum LanguageCode {
    de,
    en,
    it,
    fr
}
