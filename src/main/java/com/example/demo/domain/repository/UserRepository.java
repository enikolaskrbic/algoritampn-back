package com.example.demo.domain.repository;

import com.example.demo.controller.dto.response.UserInvoicesResponse;
import com.example.demo.controller.dto.response.UserResponse;
import com.example.demo.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User getByUsernameAndDeleted(String username, Boolean deleted);

    User getByIdAndDeleted(Long id, Boolean deleted);

    @Query(value = "SELECT new com.example.demo.controller.dto.response.UserResponse(u.id,u.code,u.name,u.username,u.email,u.apiKey,u.isEnabledMojeracun) FROM User u WHERE u.id <> :myId AND u.deleted = false")
    List<UserResponse> getUsers(@Param("myId") Long myId);

    @Query(value = "SELECT DISTINCT new com.example.demo.controller.dto.response.UserInvoicesResponse(u.id,u.code,u.name,u.username,u.isEnabledMojeracun, u.invoices.size) FROM User u JOIN Invoice i on u.id = i.user.id WHERE u.id <> :myId AND u.deleted = false AND i.uploadTime >= :fromDate AND i.uploadTime <= :toDate")
    List<UserInvoicesResponse> getUsersInvoices(@Param("myId") Long myId, @Param("fromDate") Long fromDate, @Param("toDate") Long toDate);
}

