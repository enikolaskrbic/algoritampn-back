package com.example.demo.domain.repository;

import com.example.demo.controller.dto.response.InvoiceResponse;
import com.example.demo.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    @Query(value = "SELECT new com.example.demo.controller.dto.response.InvoiceResponse(i.id, i.invoiceId,i.salesId,i.fileName, i.uploadTime, i.status)" +
            "FROM Invoice i " +
            "WHERE i.uploadTime >= :fromDate AND i.uploadTime <= :toDate AND i.user.id = :userId")
    List<InvoiceResponse> getByFromDateToDateAndUser(@Param("fromDate") Long fromDate, @Param("toDate") Long toDate, @Param("userId") Long userId);

    Invoice getBySalesId(Long salesId);

    Invoice getById(Long invoiceId);
}

