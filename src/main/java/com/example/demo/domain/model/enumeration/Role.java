package com.example.demo.domain.model.enumeration;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
