package com.example.demo.domain.model;

import com.example.demo.domain.model.enumeration.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "code")
    @Size(max = 150)
    private String code;

    @NotNull
    @Column(name = "name")
    @Size(max = 150)
    private String name;

    @NotNull
    @Column(name = "username")
    @Size(max = 30)
    private String username;

    @Column(name = "email")
    @Size(max = 50)
    private String email;

    @Column(name = "api_key")
    private String apiKey;

    @JsonIgnore
    @NotNull
    @Size(max = 128)
    @Column(name = "password")
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @NotNull
    @Column(name = "deleted")
    private Boolean deleted;

    @NotNull
    @Column(name = "is_enabled_mojeracun")
    private Boolean isEnabledMojeracun;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    Collection<Invoice> invoices;

}
