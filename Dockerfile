FROM maven:3.5-jdk-8 as maven

COPY ./pom.xml ./pom.xml

RUN mvn dependency:go-offline -B

COPY ./src ./src

RUN mvn clean install -DskipTests

FROM openjdk:8u171-jre-alpine

WORKDIR /demo-backend

ENV JAVA_OPTS=""

COPY --from=maven target/demo-0.0.1-SNAPSHOT.jar ./

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Duser.timezone=Europe/Belgrade -Djava.security.egd=file:/dev/./urandom -jar ./demo-0.0.1-SNAPSHOT.jar"]

